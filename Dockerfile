FROM node:14-alpine

WORKDIR /usr/src/app
COPY . .

RUN npm ci --only=production

CMD [ "npm", "run", "start" ]
