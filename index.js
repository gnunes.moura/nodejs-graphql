
const nconf = require('./env-config');
const server = require('./src/apollo-server');

server.listen({ port: nconf.get('PORT') }).then(({ url }) => {
  console.log(`🚀  Server ${nconf.get('service:name')} ready at ${url}`);
});
