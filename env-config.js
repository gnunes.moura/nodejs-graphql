const nconf = require('nconf');

/**
 * Hierarchical configuration with files, environment variables and command-line arguments.
 */

// 1º
nconf.overrides({
  service: {
    name: process.env.npm_package_name,
  },
});

// 2º
nconf.argv();

// 3º
nconf.env();

nconf.required(['NODE_ENV']);

if (nconf.get('NODE_ENV') !== 'production'
  && nconf.get('NODE_ENV') !== 'staging'
  && nconf.get('NODE_ENV') !== 'development') throw new Error('NODE_ENV must be "production", "homolog" or "development"');

// 4º
if (nconf.get('configPath')) {
  nconf.file(`${nconf.get('configPath')}`);
}

// 5º
nconf.defaults({
  PORT: 4001,
});

module.exports = nconf;
