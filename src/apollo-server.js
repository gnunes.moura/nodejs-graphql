

const { ApolloServer } = require('apollo-server');
const schema = require('./schema');

module.exports = new ApolloServer(schema);
